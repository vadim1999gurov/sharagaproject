#include "Table.h"
using namespace std;
void Table::Add(string &date, Type_operation type, size_t amount, string &description)
{
	this->date = date;
	this->type = type;
	this->amount = amount;
	this->description = description;
}

Table &Table::getObject()
{
	static Table object;
	return object;
}

//Name
string Table::getDate()
{
	return this->date;
}
//Deskription
string Table::getDescription()
{
	return this->description;
}
//Cost
size_t Table::getAmount()
{
	return this->amount;
}

//Eat or furniture
Table::Type_operation Table::getType()
{
	return this->type;
}
string Table::getTypeToStr()
{
	return Table::getType() == Table::income ? "Eat" : "Furniture";
}
