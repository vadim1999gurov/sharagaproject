#include "Menu.h"
Menu::Menu()
{
}

Menu &Menu::getObject()
{
	static Menu object;
	return object;
}
void Menu::inputData()
{
	string date, description, correspondent;
	Table::Type_operation type;
	char ch;
	size_t amount;
	cout << "Enter the name: ";
	cin >> date;
	cout << "Enter the type product (1 - Eat, 2 - Furniture): ";
	cin >> ch;
	ch == '1' ? type = Table::income : type = Table::expense;
	cout << "Enter the cost: ";
	cin >> amount;
	cout << "Enter the description: ";
	cin >> description;
	Table::getObject().Add(date, type, amount, description);
	list.push_back(Table::getObject());
	cout << endl
		 << "Date added successfully. Enter for back to main menu.";
	cin.get();
}
void Menu::outputData()
{
	if (!list.empty())
	{
		//cout  << "Date\t|\t"  << "Type\t|\t" << "Amount\t|\t" << "Descr.\t\t|\t" << "Corres-t\t|\n";
		for (size_t i = 0; i < list.size(); i++)
		{
			cout
				<< list[i].getDate() << "\t\t"
				<< list[i].getTypeToStr() << "\t\t"
				<< list[i].getAmount() << "\t\t"
				<< list[i].getDescription() << "\t"
				<< "(" << i << ")" << endl;
		}
	}
	else
		cout << "The list is empty.";
	cin.get();
}

void Menu::editData()
{
	if (!list.empty())
	{
		outputData();
		cout << "Enter the line number to edit: ";
		int number;
		cin >> number;
		if (number < list.size())
		{
			string date, description, correspondent;
			Table::Type_operation type;
			char ch;
			size_t amount;
			cout << "Enter the name: ";
			cin >> date;
			cout << "Enter the type product (1 - Eat, 2 - Furniture)";
			cin >> ch;
			ch == '1' ? type = Table::income : type = Table::expense;
			cout << "Enter the cost: ";
			cin >> amount;
			cout << "Enter the description: ";
			cin >> description;
			list[number].Add(date, type, amount, description);
			cout << "The line is edited successfully." << endl;
		}
		else
			cout << "Error edit, because net takoy tsifri";
	}
	else
		cout << "The list is empty.";
	cin.get();
}